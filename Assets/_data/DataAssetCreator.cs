﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class DataAssetCreator
{
    [MenuItem("Assets/Create/Custom/LevelData")]
    public static void CreateData()
    {
        ScriptableObjectUtility.CreateAsset<LevelData>();
    }


}
