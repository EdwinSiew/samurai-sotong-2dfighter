﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[System.Serializable]
public class LevelData : ScriptableObject
{

    public Movement movementData;
    public Attack attackData;
    public Camera camData;

    [Serializable]
    public class Movement
    {
        [Header("Movement Speed")]
        public float movementSpeed;
        public float jumpForce;

    }

    [Serializable]
    public class Attack
    {
        // [Header("Attack Confiq : Larger number, slower it gets")]
        // public float attackSpeed;
        public float swordSizeMultiplier;
        public float swordTransitionScaleFactor = 0.3f;
        public float impactPauseTime = 0.2f;
        public float damageScaleFactor = 1;
      
    }


    [Serializable]
    public class Camera
    {
        public float successfulParryZoomFactor;
        public float successfulParryZoomTransitionDuration;
    }

}
