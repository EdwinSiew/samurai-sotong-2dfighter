﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using DG.Tweening;
using Cinemachine;

public class GameManager : MonoBehaviour
{

    private bool p1isGrounded;
    private RigidbodyConstraints originalConstraints;
    // public GameObject explosion;



    public bool shootParticles_1;
    public bool shootParticles_2;
    public Animator playerOneAnim;
    public Animator playerOneSwordAnim;
    public Animator playerTwoAnim;
    public Animator playerTwoSwordAnim;

    private bool playerTwoAttack;
    public bool isParried_1;
    public bool isParried_2;

    public bool p1_isBomb;
    public bool p2_isBomb;
    private float swordSize = 2.4801f;
    private float dynamicOrthographicScale; //needs reset
    private int swordDynamicSizeCam;  //needs reset
    private int swordDynamicSizeCamSmoothen;  //needs reset
    private float newDamagePower = 1;
    private float damagePower;
    private float successfulParryZoom;
    private bool parriedSuccess;
    public bool p1isStunned;
    public bool p2isStunned;
    private Vector3 playerOnePosition;
    private Vector3 playerTwoPosition;
    public Vector2 doRipplePosition;

    [SerializeField]
    private LevelData levelData;
    public static GameManager Instance { get; private set; }

    public ViewManager _viewManager;
    public ViewManager ViewManager
    {
        get
        {
            return _viewManager;
        }
        set
        {
            _viewManager = value;
        }
    }

    public collisionEventHandler_p1 _collisionEventHandler_1;
    public collisionEventHandler_p1 collisionEventHandler_p1
    {
        get
        {
            return _collisionEventHandler_1;
        }
        set
        {
            _collisionEventHandler_1 = value;
        }
    }
    public collisionEventHandler_p2 _collisionEventHandler_2;
    public collisionEventHandler_p2 collisionEventHandler_p2
    {
        get
        {
            return _collisionEventHandler_2;
        }
        set
        {
            _collisionEventHandler_2 = value;
        }
    }

    public hitReg_p1 _hitReg_1;
    public hitReg_p1 hitReg_P1
    {
        get
        {
            return _hitReg_1;
        }
        set
        {
            _hitReg_1 = value;
        }
    }
    public hitReg_p2 _hitReg_2;
    public hitReg_p2 hitReg_P2
    {
        get
        {
            return _hitReg_2;
        }
        set
        {
            _hitReg_2 = value;
        }
    }

    public ballCollision _ballCollision;
    public ballCollision ballCollision
    {
        get
        {
            return _ballCollision;
        }
        set
        {
            _ballCollision = value;
        }
    }

    public RippleEffect _rippleEffect;
    public RippleEffect rippleEffect
    {
        get
        {
            return _rippleEffect;
        }
        set
        {
            _rippleEffect = value;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        _viewManager = GameObject.FindObjectOfType<ViewManager>();
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        originalConstraints = _viewManager.playerOne.constraints;
    }

    void Start()
    {
        // playerOneAnim = GetComponent<Animator>();


    }
    // Update is called once per frame
    void Update()
    {

        _viewManager.label_p1.GetComponent<Transform>().position = _viewManager.labelLasso_p1.GetComponent<Transform>().position;
        _viewManager.label_p2.GetComponent<Transform>().position = _viewManager.labelLasso_p2.GetComponent<Transform>().position;








        #region PLAYER ONE CONTROLS
        if (Input.GetKey("a"))
        {
            horizontalMovement("left", 1);
        }
        if (Input.GetKey("d"))
        {
            horizontalMovement("right", 1);
        }
        if (Input.GetKeyUp("a") || Input.GetKeyUp("d"))
        {
            STOPhorizontalMovement(1);
        }
        if (Input.GetKey("w") && collisionEventHandler_p1.p1isGrounded == true)
        {
            jump(1);
        }
        if (Input.GetKey("f"))
        {
            // playerOneAnim.speed = levelData.attackData.attackSpeed;
            // playerOneAnim.ResetTrigger("parrySwordAttack");
            StartCoroutine("attack", 1);

        }
        if (Input.GetKey("f") && _hitReg_1.hitting == true && p1_isBomb == false)
        {
            StartCoroutine("parryParticle", 1);
        }
        if (_hitReg_1.p2isHit == false && _hitReg_1.hitting == true)
        {
            // print("p2 MISSED");
            isParried_1 = false;
        }


        if (_hitReg_1.p2isHit == true && _hitReg_2.hitting == false && isParried_1 == false && p1_isBomb == true)
        {
            print("p2 DAMAGED");
            damage(2);
        }

        else if (_hitReg_1.p2SwordisHit == true && _hitReg_2.hitting == true && p1_isBomb == false && p2_isBomb == true)
        {
            isParried_1 = true;
            print("p2 parried");
            p1_isBomb = true;
            p2_isBomb = false;
            parry(1);
        }


        if (p1_isBomb == true)
        {
            playerOneSwordAnim.SetBool("theBOMB_transition", true);
        }
        else if (p1_isBomb == false)
        {
            playerOneSwordAnim.SetBool("theBOMB_transition", false);
        }


        if (!shootParticles_1 && collisionEventHandler_p1.p1isGrounded == true)
        {

            _viewManager.playerOneParticles.Play();
        }
        if (collisionEventHandler_p1.p1isGrounded == false)
        {
            _viewManager.playerOneParticles.Stop();
        }








        #endregion


        #region PLAYER TWO CONTROLS
        if (Input.GetKey("left"))
        {
            horizontalMovement("left", 2);
        }
        if (Input.GetKey("right"))
        {
            horizontalMovement("right", 2);
        }
        if (Input.GetKeyUp("left") || Input.GetKeyUp("right"))
        {
            STOPhorizontalMovement(2);
        }
        if (Input.GetKey("up") && collisionEventHandler_p2.p2isGrounded == true)
        {
            jump(2);

        }
        if (Input.GetKey("right ctrl"))
        {
            // playerOneAnim.speed = levelData.attackData.attackSpeed;
            // playerOneAnim.ResetTrigger("parrySwordAttack");
            StartCoroutine("attack", 2);
        }
        if (Input.GetKey("right ctrl") && _hitReg_2.hitting == true && p2_isBomb == false)
        {
            StartCoroutine("parryParticle", 2);
        }
        //HIT CHECKER

        if (_hitReg_2.p1isHit == false && _hitReg_2.hitting == true)
        {
            // print("p1 MISSED");
            isParried_2 = false;
        }


        if (_hitReg_2.p1isHit == true && _hitReg_1.hitting == false && isParried_2 == false && p2_isBomb == true)
        {
            print("p1 DAMAGED");
            damage(1);
        }

        else if (_hitReg_2.p1SwordisHit == true && _hitReg_1.hitting == true && p2_isBomb == false && p1_isBomb == true)
        {
            isParried_2 = true;
            print("p1 parried");
            p2_isBomb = true;
            p1_isBomb = false;
            parry(2);
        }


        if (p2_isBomb == true)
        {
            playerTwoSwordAnim.SetBool("theBOMB_transition", true);
        }
        else if (p2_isBomb == false)
        {
            playerTwoSwordAnim.SetBool("theBOMB_transition", false);
        }

        if (_ballCollision.swordInBall_2 == true)
        {
            p2_isBomb = true;
            _viewManager.powerBall.SetActive(false);
            _ballCollision.swordInBall_2 = false;
            _ballCollision.swordInBall_1 = false;

        }
        else if (_ballCollision.swordInBall_1)
        {
            p1_isBomb = true;
            _viewManager.powerBall.SetActive(false);
            _ballCollision.swordInBall_1 = false;
            _ballCollision.swordInBall_2 = false;

        }



        if (!shootParticles_2 && collisionEventHandler_p2.p2isGrounded == true)
        {

            _viewManager.playerTwoParticles.Play();
        }
        if (collisionEventHandler_p2.p2isGrounded == false)
        {
            _viewManager.playerTwoParticles.Stop();
        }
        #endregion





        #region CAMERA SETUP
        Transform playerOnePos = _viewManager.playerOneTransform;
        Transform playerTwoPos = _viewManager.playerTwoTransform;
        Transform camPos = _viewManager.camGO.GetComponent<Transform>();

        float camMiddlePointX = playerOnePos.position.x + (playerTwoPos.position.x - playerOnePos.position.x) / 2;
        float camMiddlePointY = playerOnePos.position.y + (playerTwoPos.position.y - playerOnePos.position.y) / 2;

        _viewManager.camLasso.position = new Vector3(camMiddlePointX, camMiddlePointY - 3.2f, 0);   //CAM POSITION

        Camera camSize = _viewManager.camGO.GetComponent<Camera>();
        float playerDist = Vector3.Distance(playerOnePos.position, playerTwoPos.position);
        dynamicOrthographicScale = (playerDist / 3) + 1.9f + (swordDynamicSizeCam / 6) - successfulParryZoom;
        if (dynamicOrthographicScale > 10f)
        {
            dynamicOrthographicScale = 10f;
        }
        if (parriedSuccess == true)
        {
            successfulParryZoom = Mathf.Lerp(successfulParryZoom, levelData.camData.successfulParryZoomFactor, 5);
        }
        else if (parriedSuccess == false)
        {
            successfulParryZoom = Mathf.Lerp(successfulParryZoom, 0f, 5);
        }
        _viewManager.cinemachine_cam.GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = dynamicOrthographicScale;
        print(successfulParryZoom);
        #endregion

    }

    public void horizontalMovement(string direction, int player)
    {
        if (player == 1 && direction == "left" && p1isStunned == false)
        {
            shootParticles_1 = true;
            playerOneAnim.SetBool("isMoving", true);
            // _viewManager.playerOne.constraints = RigidbodyConstraints.FreezeRotation;
            _viewManager.playerOne.constraints = originalConstraints;
            _viewManager.playerOne.AddForce((Vector3.left / 10) * levelData.movementData.movementSpeed, ForceMode.VelocityChange);
            _viewManager.playerOneGO.transform.DOLocalRotate(new Vector3(0, 180, 0), 0.2f);
            // _viewManager.playerOneGO.transform.localRotation = Quaternion.Euler(0,180,0);
        }

        if (player == 1 && direction == "right" && p1isStunned == false)
        {
            playerOneAnim.SetBool("isMoving", true);
            shootParticles_1 = true;

            // _viewManager.playerOne.constraints = RigidbodyConstraints.FreezeRotation;
            _viewManager.playerOne.constraints = originalConstraints;
            _viewManager.playerOne.AddForce((Vector3.right / 10) * levelData.movementData.movementSpeed, ForceMode.VelocityChange);
            _viewManager.playerOneGO.transform.DOLocalRotate(new Vector3(0, 0, 0), 0.2f);
            // _viewManager.playerOneGO.transform.localRotation = Quaternion.Euler(0, 0, 0);

        }


        if (player == 2 && direction == "left" && p2isStunned == false)
        {
            shootParticles_2 = true;
            playerTwoAnim.SetBool("isMoving", true);
            // _viewManager.playerOne.constraints = RigidbodyConstraints.FreezeRotation;
            _viewManager.playerTwo.constraints = originalConstraints;
            _viewManager.playerTwo.AddForce((Vector3.left / 10) * levelData.movementData.movementSpeed, ForceMode.VelocityChange);
            _viewManager.playerTwoGO.transform.DOLocalRotate(new Vector3(0, 180, 0), 0.2f);
            // _viewManager.playerOneGO.transform.localRotation = Quaternion.Euler(0,180,0);
        }

        if (player == 2 && direction == "right" && p2isStunned == false)
        {
            shootParticles_2 = true;
            playerTwoAnim.SetBool("isMoving", true);

            // _viewManager.playerOne.constraints = RigidbodyConstraints.FreezeRotation;
            _viewManager.playerTwo.constraints = originalConstraints;
            _viewManager.playerTwo.AddForce((Vector3.right / 10) * levelData.movementData.movementSpeed, ForceMode.VelocityChange);
            _viewManager.playerTwoGO.transform.DOLocalRotate(new Vector3(0, 0, 0), 0.2f);
            // _viewManager.playerOneGO.transform.localRotation = Quaternion.Euler(0, 0, 0);

        }




    }
    public void STOPhorizontalMovement(int player)
    {

        if (player == 1)
        {
            shootParticles_1 = false;

            playerOneAnim.SetBool("isMoving", false);

            _viewManager.playerOne.constraints = RigidbodyConstraints.FreezePositionX;
            _viewManager.playerOne.constraints = RigidbodyConstraints.FreezeRotation;
        }

        if (player == 2)
        {
            shootParticles_2 = false;

            playerTwoAnim.SetBool("isMoving", false);

            _viewManager.playerTwo.constraints = RigidbodyConstraints.FreezePositionX;
            _viewManager.playerTwo.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    public void jump(int player)
    {
        if (player == 1)
        {
            // shootParticles_1 = true;
            // StartCoroutine("jumpParticle", player);
            _viewManager.playerOne.AddForce(Vector3.up * levelData.movementData.jumpForce, ForceMode.VelocityChange);
            // _viewManager.playerOne.AddForce(Vector3.up * levelData.movementData.jumpForce, ForceMode.Impulse);
        }
        if (player == 2)
        {
            // shootParticles_1 = true;
            // StartCoroutine("jumpParticle", player);
            _viewManager.playerTwo.AddForce(Vector3.up * levelData.movementData.jumpForce, ForceMode.VelocityChange);
            // _viewManager.playerOne.AddForce(Vector3.up * levelData.movementData.jumpForce, ForceMode.Impulse);
        }
    }

    // IEnumerator jumpParticle(int player)
    // {
    //     if (player == 1)
    //     {
    //         _viewManager.playerOneParticles.Play();
    //         yield return new WaitForSeconds(0.05f);
    //         _viewManager.playerOneParticles.Stop();
    //     }

    // }

    IEnumerator attack(int player)
    {
        if (player == 1)
        {
            playerOneSwordAnim.SetBool("parrySwordAttack", true);

            yield return new WaitForSeconds(0.1f);

            playerOneSwordAnim.SetBool("parrySwordAttack", false);
        }
        if (player == 2)
        {
            playerTwoSwordAnim.SetBool("parrySwordAttack", true);
            // playerTwoAttack = true;
            yield return new WaitForSeconds(0.1f);
            // playerTwoAttack = false;
            playerTwoSwordAnim.SetBool("parrySwordAttack", false);
        }
    }

    public void parry(int player)
    {
        StartCoroutine("Pause", levelData.attackData.impactPauseTime);

        _viewManager.camLasso.DOShakePosition(0.4f, .9f, 14, 90, false, true);

        Vector3 defaultScaling = new Vector3(2.4801f, 2.4801f, 2.4801f);
        swordDynamicSizeCam++;
        newDamagePower++;

        // swordDynamicSizeCamSmoothen = 

        swordSize = swordSize + levelData.attackData.swordSizeMultiplier;
        Vector3 swordSizeIncrement = new Vector3(swordSize, swordSize, swordSize);
        if (player == 1)
        {

            playerTwoPosition = _viewManager.playerTwoGO.GetComponent<Transform>().position;
            FindObjectOfType<RippleEffect>().Emit(Camera.main.WorldToViewportPoint(playerOnePosition));
            _viewManager.sword_p1.GetComponent<Transform>().DOScale(swordSizeIncrement, levelData.attackData.swordTransitionScaleFactor);
            _viewManager.sword_p2.GetComponent<Transform>().DOScale(defaultScaling, levelData.attackData.swordTransitionScaleFactor);
            StartCoroutine("successfulParry", 1);
        }
        if (player == 2)
        {

            playerOnePosition = _viewManager.playerOneGO.GetComponent<Transform>().position;
            FindObjectOfType<RippleEffect>().Emit(Camera.main.WorldToViewportPoint(playerTwoPosition));
            doRipplePosition = new Vector2(playerOnePosition.x, playerOnePosition.y);
            _viewManager.sword_p2.GetComponent<Transform>().DOScale(swordSizeIncrement, levelData.attackData.swordTransitionScaleFactor);
            _viewManager.sword_p1.GetComponent<Transform>().DOScale(defaultScaling, levelData.attackData.swordTransitionScaleFactor);
            StartCoroutine("successfulParry", 2);
        }
    }



    public void damage(int player)
    {
        StartCoroutine("Pause", levelData.attackData.impactPauseTime);
        newDamagePower = newDamagePower + 0.003f;
        float damagePowerBase = -50f;
        damagePower = damagePowerBase * newDamagePower / 5 * levelData.attackData.damageScaleFactor;

        _viewManager.camLasso.DOShakePosition(0.5f, 1f, 14, 90, false, true);
        if (player == 1)
        {
            StartCoroutine("hitParticle", 1);
            _viewManager.playerOneGO.GetComponent<Rigidbody>().AddForce(new Vector3(_viewManager.playerOneGO.GetComponent<Transform>().forward.z * damagePower, 50f * -damagePower / 100, 0));
            StartCoroutine("stun", 1);
        }
        if (player == 2)
        {
            StartCoroutine("hitParticle", 2);
            _viewManager.playerTwoGO.GetComponent<Rigidbody>().AddForce(new Vector3(_viewManager.playerTwoGO.GetComponent<Transform>().forward.z * damagePower, 50f * -damagePower / 100, 0));
            StartCoroutine("stun", 2);
        }
    }
    private IEnumerator Pause(float p)
    {
        Time.timeScale = 0.1f;
        float pauseEndTime = Time.realtimeSinceStartup + p;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }
        Time.timeScale = 1;



    }
    private IEnumerator successfulParry(int player)
    {
        parriedSuccess = true;
        yield return new WaitForSeconds(levelData.camData.successfulParryZoomTransitionDuration);
        parriedSuccess = false;

        if (player == 1)
        {
            _viewManager.playerOneParriedParticles.Play();
            yield return new WaitForSeconds(levelData.camData.successfulParryZoomTransitionDuration + 0.2f);
            _viewManager.playerOneParriedParticles.Stop();
        }
        if (player == 2)
        {
            _viewManager.playerTwoParriedParticles.Play();
            yield return new WaitForSeconds(levelData.camData.successfulParryZoomTransitionDuration + 0.2f);
            _viewManager.playerTwoParriedParticles.Stop();
        }
    }

    private IEnumerator stun(int player)
    {

        if (player == 1)
        {
            p1isStunned = true;
            _viewManager.playerOneDamageParticles.Play();
            yield return new WaitForSeconds(newDamagePower / 20);
            _viewManager.playerOneDamageParticles.Stop();
            p1isStunned = false;
        }
        if (player == 2)
        {
            p2isStunned = true;
            _viewManager.playerTwoDamageParticles.Play();
            yield return new WaitForSeconds(newDamagePower / 20);
            _viewManager.playerTwoDamageParticles.Stop();
            p2isStunned = false;
        }

    }

    private IEnumerator parryParticle(int player)
    {
        if (player == 1)
        {
            _viewManager.playerOneParryParticles.Play();
            yield return new WaitForSeconds(0.2f);
            _viewManager.playerOneParryParticles.Stop();
        }
        if (player == 2)
        {
            _viewManager.playerTwoParryParticles.Play();
            yield return new WaitForSeconds(0.2f);
            _viewManager.playerTwoParryParticles.Stop();
        }
    }

    private IEnumerator hitParticle(int player)
    {
        if (player == 1)
        {
            _viewManager.playerOneHitParticles.Play();
            yield return new WaitForSeconds(0.2f);
            _viewManager.playerOneHitParticles.Stop();
        }
        if (player == 2)
        {
            _viewManager.playerTwoHitParticles.Play();
            yield return new WaitForSeconds(0.2f);
            _viewManager.playerTwoHitParticles.Stop();
        }
    }
}

