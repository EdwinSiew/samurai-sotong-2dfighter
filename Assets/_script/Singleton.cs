﻿using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T:MonoBehaviour
{
	private static T instance;

	public static T Instance {
		get {
			if (instance == null) {
				GameObject gameObject = new GameObject (typeof(T).ToString ());
				gameObject.AddComponent<T> ();
			}
			return instance;
		}
	}

	protected void Awake ()
	{
		if (instance != null) {
			//Delete for duplicated game object with Singleton class
			//Usually the case will be happen when singleton game object attached at the scene and return back to same scene again
			Destroy (gameObject);
		} else {
			instance = gameObject.GetComponent<T> ();
			DontDestroyOnLoad (gameObject);
		}
	}

	public void Destroy ()
	{
		Destroy (gameObject);
		instance = null;
	}
}