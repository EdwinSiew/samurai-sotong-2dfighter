﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewManager : MonoBehaviour
{
    public Rigidbody playerOne;
    public GameObject playerOneGO;
    public ParticleSystem playerOneParticles;
    // public Collider playerOne_groundChecker;
    public Rigidbody playerTwo;
    public GameObject playerTwoGO;
    public ParticleSystem playerTwoParticles;
    public GameObject sword_p1;
    public GameObject sword_p2;
    public ParticleSystem playerOneDamageParticles;
    public ParticleSystem playerTwoDamageParticles;
    public ParticleSystem playerOneParriedParticles;
    public ParticleSystem playerTwoParriedParticles;
    public ParticleSystem playerOneParryParticles;
    public ParticleSystem playerTwoParryParticles;
    public ParticleSystem playerOneHitParticles;
    public ParticleSystem playerTwoHitParticles;


    [Header("Camera Setup")]
    public Transform playerOneTransform;
    public Transform playerTwoTransform;
    public GameObject camGO;
    public Transform camLasso;
    public GameObject cinemachine_cam;
    public GameObject powerBall;
    
    [Header("Camera Setup")]
    public GameObject label_p1;
    public GameObject labelLasso_p1;
    public GameObject label_p2;
    public GameObject labelLasso_p2;

    
    // public Cinemachine cam;

}
