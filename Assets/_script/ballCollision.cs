﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballCollision : MonoBehaviour
{
    public bool swordInBall_1;
    public bool swordInBall_2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player_1")
        {
            swordInBall_1 = true;
            // print("in");
        }
        else if (other.tag == "Player_2")
        {
            swordInBall_2 = true;
            // print("in");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player_1")
        {
            // print("out");
            swordInBall_1 = false;
        }
        else if (other.tag == "Player_2")
        {
            // print("out");
            swordInBall_1 = false;
        }
    }
}
