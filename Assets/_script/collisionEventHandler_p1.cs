﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionEventHandler_p1 : MonoBehaviour
{

    public bool p1isGrounded;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ground")
        {
            p1isGrounded = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ground")
        {
            p1isGrounded = false;
        }
    }

}
