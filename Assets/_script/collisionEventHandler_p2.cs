﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionEventHandler_p2 : MonoBehaviour
{

    public bool p2isGrounded;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ground")
        {
            p2isGrounded = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ground")
        {
            p2isGrounded = false;
        }
    }

}
