﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitReg_p1 : MonoBehaviour
{

    private bool isP2inHitZone;
    public bool hitting;
    public bool p2isHit;
    private bool isP2SwordinHitZone;
    public bool p2SwordisHit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player_2")
        {
            isP2inHitZone = true;
        }
        if (other.tag == "Sword_2")
        {
            isP2SwordinHitZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player_2")
        {
            isP2inHitZone = false;
        }

        if (other.tag == "Sword_2")
        {
            isP2SwordinHitZone = false;
        }
    }

    void Update()
    {
        // print(isP2inHitZone);
        if (hitting == true)
        {
            if (isP2inHitZone)
            {
                p2isHit = true;
            }
            if (!isP2inHitZone)
            {
                p2isHit = false;
            }
            if (isP2SwordinHitZone)
            {
                p2SwordisHit = true;
            }
            if (!isP2SwordinHitZone)
            {
                p2SwordisHit = false;
            }
        }
        if (hitting == false)
        {
            p2isHit = false;
            p2SwordisHit = false;
        }
    }
}
