﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitReg_p2 : MonoBehaviour
{

    private bool isP1inHitZone;
    public bool hitting;
    public bool p1isHit;
    private bool isP1SwordinHitZone;
    public bool p1SwordisHit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player_1")
        {
            isP1inHitZone = true;
        }
        if (other.tag == "Sword_1")
        {
            isP1SwordinHitZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player_1")
        {
            isP1inHitZone = false;
        }
        if (other.tag == "Sword_1")
        {
            isP1SwordinHitZone = false;
        }
    }

    void Update()
    {
        if (hitting == true)
        {
            if (isP1inHitZone)
            {
                p1isHit = true;
            }
            if (!isP1inHitZone)
            {
                p1isHit = false;
            }
            if (isP1SwordinHitZone)
            {
                p1SwordisHit = true;
            }
            if (!isP1SwordinHitZone)
            {
                p1SwordisHit = false;
            }
        }
        if (hitting == false)
        {
            p1isHit = false;
            p1SwordisHit = false;
        }

    }
}
